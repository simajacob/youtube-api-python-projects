# https://github.com/googleapis/google-api-python-client
# https://github.com/googleapis/google-api-python-client/blob/master/docs/README.md
# https://github.com/googleapis/google-api-python-client/blob/master/docs/start.md
# http://googleapis.github.io/google-api-python-client/docs/epy/googleapiclient.discovery-module.html#build
# https://github.com/googleapis/google-api-python-client/blob/master/docs/dyn/index.md
# http://googleapis.github.io/google-api-python-client/docs/dyn/youtube_v3.html
# http://googleapis.github.io/google-api-python-client/docs/dyn/youtube_v3.channels.html
# https://developers.google.com/youtube/v3/docs/channels/list



from googleapiclient.discovery import build
import os
import re
from datetime import timedelta

print(os.environ)
api_key = os.environ['YOUTUBE_API_PYTHON']
youtube = build('youtube','v3',developerKey=api_key)

# # Get channels statistics and more content details
# # from username get the channelId of a particular channel
# ch_request = youtube.channels().list(
#   part='contentDetails, statistics',
#   forUsername='schafer5'
# )
# response = ch_request.execute()
# print(response)


# # Use the channel id known, find the snippet of this channel
# # Get playlists
# pl_request = youtube.playlists().list(
#   part='contentDetails, snippet',
#   channelId="UCCezIgC97PvUuR4_gbFUs5g"
# )
# pl_response = pl_request.execute()
# # print(pl_response)
# for item in pl_response['items']:
#   print(item)
#   print('')




# Get a specific playlist from playlists from the playlistId selected
spl_request = youtube.playlistItems().list(
  part='contentDetails',
  playlistId="PL-osiE80TeTsWmV9i9c58mdDCSskIFdDS"
)
spl_response = spl_request.execute()
# print(pl_response)

# This loop get 5 first video ids of the playlist and create a new vid_ids list
# Using list comprehension
vid_ids = list(item['contentDetails']['videoId']  for item in spl_response['items'])
# ['ZyhVh-qRZPA', 'zmdjNSmRXF4', 'W9XjRYFkkyw', 'Lw2rlcxScZY', 'DCDe29sIKcE']



# request each video resources, pass video id or a multiple comma separeted video id string
# use join method on a list to do this operation
vid_request = youtube.videos().list(
  part = 'contentDetails',
  id = ','.join(vid_ids)    # less than 50 id allowed
)

vid_response = vid_request.execute()

hours_pattern  = re.compile(r'(\d+)H')     # get one or more digit group before H
minutes_pattern  = re.compile(r'(\d+)M')   # get one or more digit group before M
seconds_pattern  = re.compile(r'(\d+)S')   # get one or more digit group before S

for item in vid_response['items']:
  duration = item['contentDetails']['duration']

  hours = hours_pattern.search(duration) 
  minutes = minutes_pattern .search(duration) 
  seconds = seconds_pattern.search(duration) 
  # print(hours,minutes,seconds)
  # None <re.Match object; span=(2, 5), match='23M'> <re.Match object; span=(5, 7), match='1S'>


  hours = int(hours.group(1)) if hours else 0
  minutes = int(minutes.group(1)) if minutes else 0
  seconds = int(seconds.group(1)) if seconds else 0
  # print(hours,minutes,seconds)

  # get the total seconds of everything that has been passed in
  video_seconds = timedelta(
      hours = hours,
      minutes = minutes,
      seconds= seconds
  ).total_seconds()

  print(video_seconds)



 





