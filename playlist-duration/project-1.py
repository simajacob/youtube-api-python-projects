
from googleapiclient.discovery import build
import re
import os
from datetime import timedelta

api_key = os.environ['YOUTUBE_API_PYTHON']
youtube = build('youtube','v3',developerKey=api_key)

hours_pattern  = re.compile(r'(\d+)H')     # get one or more digit group before H
minutes_pattern  = re.compile(r'(\d+)M')   # get one or more digit group before M
seconds_pattern  = re.compile(r'(\d+)S')   # get one or more digit group before S

total_seconds = 0
nextPageToken = None
while True: 
  # Get a specific playlist from playlists from the playlistId selected
  spl_request = youtube.playlistItems().list(
    part='contentDetails',
    # playlistId="PL-osiE80TeTsWmV9i9c58mdDCSskIFdDS",    # Corey schafer pandas playlist id
    playlistId="PL-osiE80TeTt2d9bfVyTiXJA-UTHn6WwU",   # Corey schafer python playlist id
    maxResults = 50,
    pageToken = nextPageToken  # this will give the first page of result
  )
  spl_response = spl_request.execute()


  # Using list comprehension
  vid_ids = list(item['contentDetails']['videoId']  for item in spl_response['items'])

  # request each video resources, pass video id or a multiple comma separeted video id string
  # use join method on a list to do this operation
  vid_request = youtube.videos().list(
    part = 'contentDetails',
    id = ','.join(vid_ids)  
  )

  vid_response = vid_request.execute()

  for item in vid_response['items']:
    duration = item['contentDetails']['duration']

    hours = hours_pattern.search(duration) 
    minutes = minutes_pattern .search(duration) 
    seconds = seconds_pattern.search(duration) 


    hours = int(hours.group(1)) if hours else 0
    minutes = int(minutes.group(1)) if minutes else 0
    seconds = int(seconds.group(1)) if seconds else 0

    # get the total seconds of everything that has been passed in
    video_seconds = timedelta(
        hours = hours,
        minutes = minutes,
        seconds= seconds
    ).total_seconds()
    total_seconds += video_seconds 

  nextPageToken = spl_response.get('nextPageToken')
  if not nextPageToken:
    break

total_seconds = int(total_seconds)
minutes,seconds = divmod(total_seconds,60)
hours,minutes = divmod(minutes,60)
print(f'{hours}:{minutes}:{seconds}')


  





 





